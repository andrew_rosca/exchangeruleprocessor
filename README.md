# Intro #

Microsoft Exchange supports a very limited number of message handling rules. This simple command line tools reads messages from the inbox and applies regular expression matches to move each message to an arbitrary folder.

Usage:

```
ExchangeRuleProcessor.exe rules.json my.name@domain.com
```


### Notes & Remarks ###

* The current version only supports Active Directory authentication and uses the current executing user account to authenticate against Exchange. (It can be modified fairly easily to support more authentication methods. See https://msdn.microsoft.com/en-us/library/office/dn626019(v=exchg.150).aspx)
* The current version only supports moving messages to a specified folder. More actions can be added easily (delete, permanently delete, etc.)
* The target folder is searched by name and the first matching folder is returned. If you have subfolders with the same name in different folders and the first one is not your intended target, you need to modify the code to perform a "smarter" search.

# Getting Started #

* open the project in Visual Studio
* modify the rules.json file to configure rules that match your needs
* edit the project properties and change the sample email address to your own
* compile and execute

Note: when building in Debug mode, the rules.json file is automatically copied to the /bin/Debug output folder. The executable will look for this file in the folder it is executing from

# Running in "production" #

* compile the executable in Release mode
* copy the content of the /bin/Release folder to a permanent location (e.g. C:\ExchangeRuleProcessor\)
* schedule a Windows task to run the executable periodically and pass the path of the rule file and your email address as parameters. Examples:

    * ExchangeRuleProcessor.exe rules.json my.name@domain.com
    * ExchangeRuleProcessor c:\rules.json first.last@microsoft.com

# Rule Definitions #

Rules are defined in a JSON document and consist of:

* [optional] one or several regular expressions that are matched against email properties (see below)
* [required] an action (e.g. Move; currently the only action supported)
* [required for Move] a target folder to move the message to

Example Rule (JSON format):

    { "SubjectPattern" : "^Test Message$", "Action" : "Move", "TargetFolder" : "Deleted Items" }

## Supported Rule Email Properties ###

The following rule properties are currently supported:

* SubjectPattern
* FromPattern
* ToPattern
* CcPattern
* BccPattern

NOTE: for TO, CC, and BCC fields the pattern is applied to each email address if more than one is present. If at least one email address matches, the pattern is considered a successful match.