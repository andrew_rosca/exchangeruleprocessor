﻿using Microsoft.Exchange.WebServices.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRuleProcessor
{
    public class MailManager
    {
        static ExchangeService _ExchangeService = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
        static Dictionary<string, FolderId> _FolderIdCache = new Dictionary<string, FolderId>();
        static Logger Log = LogManager.GetCurrentClassLogger();

        public void Connect(EmailAddress mailboxAddress)
        {
            _ExchangeService.UseDefaultCredentials = true;
            _ExchangeService.AutodiscoverUrl(mailboxAddress.Address, RedirectionUrlValidationCallback);

            Log.Info("Connected to mailbox {0}", mailboxAddress);
        }

        public void ProcessInbox(IEnumerable<MailMessageHandlingRule> rules) { ProcessFolder(WellKnownFolderName.Inbox, rules); }

        public void ProcessFolder(WellKnownFolderName folder, IEnumerable<MailMessageHandlingRule> rules)
        {
            int pageSize = 100;         
            List<EmailMessage> emails = new List<EmailMessage>();
            FindItemsResults<Item> findResults;
            var view = new ItemView(pageSize, 0, OffsetBasePoint.Beginning);
            view.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
            view.Traversal = ItemTraversal.Shallow;

            do
            {
                findResults = _ExchangeService.FindItems(WellKnownFolderName.Inbox, view);

                foreach (Item item in findResults.Items)
                {
                    if (item is EmailMessage)
                        emails.Add((EmailMessage)item);

                    Debug.WriteLine(item.Subject);
                }

                view.Offset += pageSize;
            }
            while (findResults.MoreAvailable);

            var propertiesToFetch = new PropertySet(EmailMessageSchema.Sender, EmailMessageSchema.ToRecipients, EmailMessageSchema.CcRecipients, EmailMessageSchema.BccRecipients, EmailMessageSchema.Subject);
            _ExchangeService.LoadPropertiesForItems(emails, propertiesToFetch);

            Log.Info("Found {0} messages in folder '{1}'", emails.Count, folder);

            ProcessEmailMessages(emails, rules);
        }

        protected void ProcessEmailMessages(IEnumerable<EmailMessage> emails, IEnumerable<MailMessageHandlingRule> rules)
        {
            foreach (var email in emails)
                ProcessEmailMessage(email, rules);
        }

        protected void ProcessEmailMessage(EmailMessage email, IEnumerable<MailMessageHandlingRule> rules)
        {
            Log.Debug("Processing message with subject '{0}' from {1}", email.Subject, email.Sender);

            foreach (var rule in rules)
                if (ApplyRule(rule, email))
                    return;

            Log.Debug("No matching rules");
        }

        private bool ApplyRule(MailMessageHandlingRule rule, EmailMessage email)
        {
            if (rule.IsMatch(email))
            {
                Log.Info("Rule {{{0}}} matches message with subject '{1}'", rule, email.Subject);

                switch (rule.Action)
                {
                    case ItemHandlingAction.Move:
                        MoveItem(email, rule.TargetFolder);
                        break;
                    default:
                        Log.Error("Action '{0}' is not supported. Skipping message.", rule.Action); //soft fail (no exception) since it's not critical to stop; just skip this message
                        break;
                }
            }
       
            return false;
        }

        private void MoveItem(EmailMessage email, string target)
        {
            if (target == null)
                throw new ArgumentNullException("target", "A target has not been specified for the Move operation");

            var folderID = FindFolder(target);

            if (folderID != null)
            {
                email.Move(folderID);
                Log.Info("Moved message with subject '{0}' to '{1}'", email.Subject, target);
            }
        }

        private FolderId FindFolder(string folderName)
        {
            if (folderName == null)
                throw new ArgumentNullException("folderName");

            if (_FolderIdCache.ContainsKey(folderName))
                return _FolderIdCache[folderName];

            int pageSize = 100;
            FindFoldersResults findResults;
            var view = new FolderView(pageSize);
            view.PropertySet = new PropertySet(BasePropertySet.IdOnly, FolderSchema.DisplayName);
            view.Traversal = FolderTraversal.Deep;

            do
            {
                findResults = _ExchangeService.FindFolders(WellKnownFolderName.Root, view);

                foreach (Folder folder in findResults.Folders)
                    if (folder.DisplayName == folderName)
                    {
                        _FolderIdCache.Add(folderName, folder.Id);
                        return folder.Id;
                    }

                view.Offset += pageSize;
            }
            while (findResults.MoreAvailable);

            Log.Error("Folder {0} not found. Skipping message.", folderName); //soft fail (no exception) since it's not critical to stop; just skip this message

            return null;
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }
    }
}
