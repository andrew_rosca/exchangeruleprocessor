﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRuleProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine(@"Usage: ExchangeRuleProcessor <rulefile.json> <mailbox address>\n\nExample: ExchangeRuleProcessor rules.json first.last@domain.com");
                return;
            }

            try
            {
                var rules = ReadRules(args[0]);

                var manager = new MailManager();
                manager.Connect(args[1]);
                manager.ProcessInbox(rules);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            //Console.ReadKey();
        }

        private static IEnumerable<MailMessageHandlingRule> ReadRules(string fileName)
        {
            if (fileName == null)
                fileName = "rules.json";

            if (!File.Exists(fileName))
                fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);

            if (!File.Exists(fileName))
                throw new FileNotFoundException("Rule file not found", fileName);

            using (StreamReader file = File.OpenText(fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                return serializer.Deserialize<IEnumerable<MailMessageHandlingRule>>(new JsonTextReader(file));
            }
        }
    }
}
