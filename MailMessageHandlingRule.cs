﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExchangeRuleProcessor
{
    public class MailMessageHandlingRule : ItemHandlingRule
    {
        /// <summary>
        /// Defines a regular expression that is applied to the Sender email address of the item.
        /// </summary>
        public string FromPattern { get; set; }

        /// <summary>
        /// Defines a regular expression that is applied to each of the email addresses in the Recipients property of the item.
        /// </summary>
        public string ToPattern { get; set; }

        /// <summary>
        /// Defines a regular expression that is applied to each of the email addresses in the CcRecipients property of the item.
        /// </summary>
        public string CcPattern { get; set; }


        /// <summary>
        /// Defines a regular expression that is applied to each of the email addresses in the BccRecipients property of the item.
        /// </summary>
        public string BccPattern { get; set; }

        public bool IsMatch(EmailMessage email)
        {
            return IsRegexMatch(FromPattern, email.Sender.Address)
                && IsRegexMatchAny(ToPattern, email.ToRecipients.Select(r => r.Address))
                && IsRegexMatchAny(CcPattern, email.CcRecipients.Select(r => r.Address))
                && IsRegexMatchAny(BccPattern, email.BccRecipients.Select(r => r.Address))
                && base.IsMatch(email);
        }

        public override string ToString()
        {
            return string.Format("{0}: s='{1}' AND f='{2}' AND t='{3}' AND cc='{4}' AND bcc='{5}'", Action, SubjectPattern, FromPattern, ToPattern, CcPattern, BccPattern);
        }
    }
}
