﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExchangeRuleProcessor
{
    public class ItemHandlingRule
    {
        /// <summary>
        /// Defines a regular expression that is applied to the Subject property of the item.
        /// </summary>
        public string SubjectPattern { get; set; }
        /// <summary>
        /// Defines what should happen to the item if the conditions match
        /// </summary>
        public ItemHandlingAction Action { get; set; }

        /// <summary>
        /// Defines the target of the rule action
        /// </summary>
        /// <remarks>
        /// May be null if the action does not require a target
        /// </remarks>
        public string TargetFolder { get; set; }

        protected bool IsMatch(Item item)
        {
            return IsRegexMatch(SubjectPattern, item.Subject);
        }

        protected bool IsRegexMatch(string pattern, string input)
        {
            if (pattern == null)
                return true;

            if (string.IsNullOrEmpty(input) && pattern != "")
                return false;

            var regex = new Regex(pattern);

            return regex.IsMatch(input);
        }

        protected bool IsRegexMatchAny(string pattern, IEnumerable<string> inputs)
        {
            if (pattern == null)
                return true;

            if (inputs.Count() == 0 && pattern != "")
                return false;

            foreach (var input in inputs)
                if (IsRegexMatch(pattern, input))
                    return true;

            return false;
        }
    }
}
